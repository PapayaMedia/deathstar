PGraphics g1, g2;
PVector posicionCamara;
int ancho, alto, camara = 0;
Juego juego = new Juego(1);
float paso = PI / 180;

void setup() {
  ancho = 800;
  alto = 600;
  size(ancho, alto);
  g1 = createGraphics(ancho, alto, P3D);
  g2 = createGraphics(250, 50, P2D);
  
  plotG1();
  plotG2();
}

void draw() {
  
  plotG1();
  plotG2();
  
  image(g1, 0, 0, ancho, alto);
  image(g2, 0, 0, 250, 50);
}

void plotG1() { // Pantalla del Juego
  Arma arma = juego.getArmaSeleccionada();
  float phi = juego.getNavePhi(), theta = juego.getNavetheta();
  juego.actualizarAsteroides(phi, theta);
  g1.beginDraw();
  g1.background(0);
  
  float camaraX = sin(phi) * -cos(theta);
  float camaraY = cos(phi);
  float camaraZ = sin(phi) * sin(theta);
  
  if (camara % 2 == 0) { // Configurar cámara
    g1.camera(101 * camaraX, 101 * camaraY, 101 * camaraZ,
           200 * camaraX, 200 * camaraY, 200 * camaraZ,
           0,1,0);
  }
  else {
    g1.camera(400, -350, 450, // Pendiente calcular la posición de la cámara
           0,0,0,
           0,1,0);
  }
  
  // INICIO PINTAR NAVE
  g1.pushMatrix();
  g1.fill(200);
  g1.noStroke();
  g1.noFill();
  g1.stroke(255, 0, 0);
  g1.rotateY(juego.getNavetheta());
  g1.rotateZ(juego.getNavePhi());
  g1.box(100);
  color colorarma = arma.getColor();
  if (camara % 2 == 0) {
    g1.stroke(colorarma);
    g1.point(camaraX, camaraY, camaraZ);
  }
  else {
    g1.stroke(colorarma);
    g1.line(0, 0, 0, 0, 500, 0);
  }
  g1.popMatrix();
  // FIN PINTAR NAVE
  
  // INICIO PINTAR ASTEROIDES
  ArrayList asteroides = juego.getAsteroides();
  for (int i = 0; i < juego.getAsteroides().size(); i++) {
    PVector asteroide = juego.getAsteroides().get(i).getPosicion();
    g1.pushMatrix();
    int estadoAsteroide = juego.getAsteroides().get(i).getEstado();
    if (estadoAsteroide >= 0) {
      color colorAsteroide = color(0);
      switch (estadoAsteroide) {
        case 0:
          colorAsteroide = color(0, 0, 255);
          break;
        case 1:
          colorAsteroide = color(0, 255, 0);
          break;
        case 2:
          colorAsteroide = color(255, 255, 0);
          break;
        default:
          colorAsteroide = color(255, 0, 0);
      }
      g1.fill(colorAsteroide);
      g1.noStroke();
      //g1.noFill();
      //g1.stroke(0, 0, 255);
      float r = asteroide.x;
      float phia = asteroide.y;
      float thetaa = asteroide.z;
      float x = r * sin(phia) * sin(thetaa);
      float y = r * cos(phia);
      float z = r * sin(phia) * cos(thetaa);
      g1.translate(x, y, z);
      g1.box(10);
    }
    g1.popMatrix();
  }
  //FIN PINTAR ASTEROIDES
  
  
  g1.endDraw();
}

void plotG2() { // Armas
  g2.beginDraw();
  for (int i = 0; i < 5; i++) {
    Arma arma = juego.getArma(i);
    color colorarma = arma.getColor();
    boolean estado = arma.esSeleccionada();
    if (estado) {
      g2.fill(colorarma);
    }
    else {
      g2.fill(0);
    }
    g2.stroke(colorarma);
    int x = 50 * i;
    g2.rect(x, 0, 49, 49);
  }
  g2.endDraw();
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      juego.updateNavePhi(1);
    } else if (keyCode == DOWN) {
      juego.updateNavePhi(-1);
    }
    else if (keyCode == LEFT) {
      juego.updateNavetheta(1);
    }
    else if (keyCode == RIGHT) {
      juego.updateNavetheta(-1);
    }
  }
  else if (key == 'v' || key == 'V') {
    camara++;
  }
  else if (key == '1') {
    juego.seleccionarArma(0);
  }
  else if (key == '2') {
    juego.seleccionarArma(1);
  }
  else if (key == '3') {
    juego.seleccionarArma(2);
  }
  else if (key == '4') {
    juego.seleccionarArma(3);
  }
  else if (key == '5') {
    juego.seleccionarArma(4);
  }
}

// CLASES DEL JUEGO

class Juego {
  Nave nave;
  int nivel;
  ArrayList<Asteroide> asteroides;
  
  Juego (int nuevoNivel) {
    nivel = nuevoNivel;
    nave = new Nave();
    asteroides = new ArrayList(0);
    for (int i = 0; i < nivel * 100; i++) {
      asteroides.add(new Asteroide());
    }
  }
  
  float getNavePhi() {
    return this.nave.getPhi();
  }
  
  float getNavetheta() {
    return this.nave.gettheta();
  }
  
  void updateNavePhi(float deltaPhi) {
    this.nave.updatePhi(deltaPhi); 
  }
  
  void updateNavetheta(float deltatheta) {
    this.nave.updatetheta(deltatheta); 
  }
  
  ArrayList<Asteroide> getAsteroides() {
    return this.asteroides;
  }
  
  Arma getArma(int i) {
    return this.nave.getArma(i);
  }
  
  Arma getArmaSeleccionada() {
    return this.nave.getArmaSeleccionada();
  }
  
  void seleccionarArma(int i) {
    this.nave.seleccionarArma(i);
  }
  
  void actualizarAsteroides(float phi, float theta) {
    for (int i = 0; i < this.asteroides.size(); i++) {
      asteroides.get(i).actualizarEstado(phi, theta);
    }
  }
}

class Nave {
  float phi, theta;
  Arma armas[];
  
  Nave () {
    phi = PI / 2;
    theta = 0;
    armas = new Arma[5];
    for (int i = 0; i < 5; i++) {
      armas[i] = new Arma(i);
    }
  }
  
  float getPhi() {
    return this.phi;
  }
  
  float gettheta() {
    return this.theta;
  }
  
  void updatePhi(float deltaPhi) {
    this.phi = max(0, min(PI, this.phi + deltaPhi * paso));
  }
  
  void updatetheta(float deltatheta) {
    this.theta = (this.theta + deltatheta * paso) % (2 * PI);
  }
  
  Arma getArma(int i) {
    return this.armas[i];
  }
  
  Arma getArmaSeleccionada() {
    for (int j = 0; j < 5; j++) {
      if (this.armas[j].esSeleccionada()) {
        return this.armas[j];
      }
    }
    return this.armas[0];
  }
  
  void seleccionarArma(int i) {
    for (int j = 0; j < 5; j++) {
      this.armas[j].seleccionar(false);
    }
    this.armas[i].seleccionar(true);
  }
}

class Asteroide {
  PVector posicion;
  float energia;
  int estado;
  
  Asteroide () {
    float phi = random(9 * PI / 20, 11 * PI / 20);
    float theta = random(0, 2 * PI);
    float r = random(300,400);
    posicion = new PVector(r, phi, theta);
    estado = 0;
    energia = 20;
  }
  
  PVector getPosicion() {
    return this.posicion; 
  }
  
  void actualizarEstado(float phi, float theta) {
    float margen = PI/20;
    if (this.energia <= 0) {
      this.estado = -1;
    }
    else {
      float deltaTheta = calcularDiferencia(theta, this.posicion.z);
      if (deltaTheta <= margen) {
        this.estado = 1;
        println ("Theta: " + theta + " - thetaA: " + this.posicion.z + " - delta: " + deltaTheta);
      }
      else {
        this.estado = 0;
      }
    }
  }
  
  int getEstado() {
    return this.estado;
  }
}

class Arma {
  float dano;
  color colorarma;
  boolean seleccionada;
  
  Arma (int i) {
    seleccionada = false;
    dano = i;
    switch (i) {
      case 0:
        colorarma = color(255);
        break;
      case 1:
        colorarma = color(0, 0, 255);
        break;
      case 2:
        seleccionada = true;
        colorarma = color(0, 255, 0);
        break;
      case 3:
        colorarma = color(255, 255, 0);
        break;
      case 4:
        colorarma = color(255, 0, 0);
        break;
    }
  }
  
  color getColor() {
    return this.colorarma; 
  }
  
  boolean esSeleccionada() {
    return this.seleccionada;
  }
  
  void seleccionar(boolean estado) {
    this.seleccionada = estado;
  }
}

float calcularDiferencia(float a, float b) {
  float diferencia = (a - b) % (2*PI);
  if (diferencia > PI) {
    diferencia = (2*PI - diferencia);
  }
  return diferencia;
}

float calcularAngulo(float angulo) {
  angulo = angulo % 2*PI;
  if (angulo >= PI) {
    angulo = -(2 * PI - angulo);
  }
  return angulo;
}
